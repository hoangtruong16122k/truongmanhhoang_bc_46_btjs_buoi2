/**Bt1
 * Input: số ngày làm
 * Output: tổng tiền lương
 */
function tongLuong(){
    var luongMotNgay = 100000;
    var soNgayLam = document.getElementById("so-ngay-lam").value * 1;
    var result = luongMotNgay * soNgayLam;
    document.getElementById("result").innerText = `Tổng lương : ${result} VND`;
}
 /**Bt3
  * Input: nhập tiền đô
  * Output: tiền quy đổi sang VND
  */
 function quyDoi(){
    var tienDo = 23500;
    var tienCanDoi = document.getElementById("tien-can-doi").value * 1;
    var tienquydoi = tienDo * tienCanDoi;
    document.getElementById("tienquydoi").innerText = `Tiền đổi : ${tienquydoi} VND`;
 }

 /**Bt4
  * Input: chiều dài và chiều rộng
  * Output: chu vi và diện tích
  */
 function chuVi(){
    var chieuDai = document.getElementById("chieu-dai").value * 1;
    var chieuRong = document.getElementById("chieu-rong").value * 1;
    var perimeter = (chieuDai + chieuRong) * 2;
    document.getElementById("perimeter").innerText = `Chu vi : ${perimeter}`;
 }
 function dienTich(){
    var chieuDai = document.getElementById("chieu-dai").value * 1;
    var chieuRong = document.getElementById("chieu-rong").value * 1;
    var acreage = chieuDai * chieuRong;
    document.getElementById("acreage").innerText = `Diện tích : ${acreage}`;
 }

 /**Bt5
  * Input: 2 chữ số
  * Output: tổng 2 chữ số
  */
